#!/bin/bash

behat=0
php=0
cont=0
total=0

./test/behat/bin/behat --version --config ./test/behat/behat.yml | grep '3.5.0' &> /dev/null

if [ $? == 0 ]; then
   echo "- Behat version is 3.5.0"
else
   echo "- Behat version should be 3.5.0"
   behat=1
fi

php -v | grep '7.3.2' &> /dev/null

if [ $? == 0 ]; then
   echo "- PHP version is 7.3.2"
else
   echo "- PHP version should be 7.3.2"
   php=1
fi

./test/behat/bin/behat -dl --config ./test/behat/behat.yml | grep 'default | Then I must see :arg1' &> /dev/null
if [ $? == 0 ]; then
   echo "- All Behat Contexts have been set."
else
   echo "- Behat contexts could not be set."
   cont=1
fi

total=$behat+$php+$cont+$dep

if (("$total" > "0")); then
   echo -e "\n>>> Problem with CHECKS !!! <<<\n"
   exit 1
fi
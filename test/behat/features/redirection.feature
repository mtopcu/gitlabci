
@redirection
Feature: Selenium js tests on Apache devbox

  Scenario: Check the links and redirections
    Given I am on "site1.html"
    Then print current URL
    When I must see "Mjolnir"
    And I follow "Thor Page"
    Then I should get redirected to "http://localhost:80/site2.html"
    And I should see "Norse God"
    When I follow "Mjolnir Page"
    Then I should get redirected to "http://localhost:80/site1.html"
    And I must see "Mjolnir is the hammer of Thor"
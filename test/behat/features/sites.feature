
@thor
Feature: Tests on Apache devbox

  Scenario Outline: Check main page items
    Given I am on "<site>.html"
    Then print current URL
    Then I must see "<expected>"

    Examples: URLs for each site of Thor php webs
      | site  | expected |
      | site1 | Mjolnir  |
      | site2 | Thor     |

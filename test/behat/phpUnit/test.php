<?php
use PHPUnit\Framework\TestCase;

class PipeTest extends TestCase
{
    public function testEmpty()
    {
        $stack = [];
        $this->assertEmpty($stack);

        return $stack;
    }

    public function testNotEmpty()
    {
        $stack = ['foo','bar'];
        $this->assertNotEmpty($stack);

        return $stack;
    }

    public function testKey()
    {
        $this->assertArrayHasKey('bar', ['bar' => 'baz']);
    }

    public function testSubset()
    {
        $this->assertArraySubset(['config' => ['key-a']], ['config' => ['key-a', 'key-b']]);
    }
}
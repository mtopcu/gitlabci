# gitlabci

In this project, the Apache server has been used as a web server to serve the HTML files. PHPUnit has been used for unit tests and Behat has been used for functional automation tests.

GitlabRunner for running on your locale:

gitlab-runner exec docker "Check Locales" --docker-volumes "/Users/trtopcmu/gitlab-pipe:/builds/mtopcu/gitlabci:rw"